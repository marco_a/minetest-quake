minetest.register_on_joinplayer(function(player)

  local p_name = player:get_player_name()

  -- se non è nello storage degli achievement, lo aggiungo
  if not achievements_lib.is_player_in_storage(p_name, "quake") then
    achievements_lib.add_player_to_storage(p_name, "quake")
  end

  -- se non è nello storage della mod, lo aggiungo
  if not quake.is_player_in_storage(p_name) then
    quake.add_player_to_storage(p_name)
  end

  -- genero l'HUD per gli achievement
  quake.HUD_achievements_create(p_name)

  player:get_meta():set_int("quake_weap_delay", 0)

  -- resetto la velocità se non c'è hub_manager
  if not minetest.get_modpath("hub_manager") then
    player: set_physics_override({
              speed = 1,
              jump = 1,
              gravity = 1,
            })
  end

  local inv = player:get_inventory()

  -- non è possibile modificare l'inventario da offline. Se sono crashati o hanno chiuso il gioco in partita,
  -- questo è l'unico modo per togliere loro l'arma
  inv:remove_item("main", ItemStack("quake:railgun"))
  inv:remove_item("main", ItemStack("quake:rocket_launcher"))
  inv:remove_item("main", ItemStack("quake:match_over"))

end)
