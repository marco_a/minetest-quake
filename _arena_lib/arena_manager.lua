local S = minetest.get_translator("quake")



arena_lib.on_load("quake", function(arena)

  for pl_name, stats in pairs(arena.players) do

    quake.HUD_stats_create(pl_name)
    quake.HUD_broadcast_create(pl_name)
    quake.scoreboard_create(arena, pl_name)

    panel_lib.get_panel(pl_name, "quake_stats"):show()

    local msg_to_display = ""

    if arena.teams_enabled then
      quake.HUD_teams_score_create(pl_name)
      panel_lib.get_panel(pl_name, "quake_teams_score"):show()
      msg_to_display = S("Team") .. ": " .. arena_lib.mods["quake"].teams[stats.teamID] .. "\n" .. S("Kills to win") .. ": " .. arena.kill_cap
    else
      msg_to_display = S("Kills to win") .. ": " .. arena.kill_cap
    end

    minetest.sound_play("quake_voice_countdown", {
      to_player = pl_name,
    })

    -- non crea E aggiorna l'HUD al tempo stesso, dacché l'after...
    minetest.after(0.1, function()
      quake.HUD_broadcast_player(pl_name, msg_to_display, 4.9)
    end)

  end


  minetest.after(0.01, function()
    quake.scoreboard_update(arena)
  end)

end)



arena_lib.on_start("quake", function(arena)

  local weapon = ItemStack("quake:railgun")
  local weapon2 = ItemStack("quake:rocket_launcher")

  for pl_name, stats in pairs(arena.players) do

    local player = minetest.get_player_by_name(pl_name)

    player:get_meta():set_int("quake_weap_delay", 0)

    player:get_inventory():add_item("main", weapon)
    player:get_inventory():add_item("main", weapon2)

    player:set_physics_override({
              speed = 2.3,
              jump = 1.5,
              gravity = 1.15,
              sneak_glitch = true,
              new_move = true
              })

    minetest.sound_play("quake_voice_fight", {
      to_player = pl_name,
    })
  end
end)



arena_lib.on_join("quake", function(p_name, arena)

  local player = minetest.get_player_by_name(p_name)
  local weapon = ItemStack("quake:railgun")
  local weapon2 = ItemStack("quake:rocket_launcher")

  quake.HUD_stats_create(p_name)
  quake.HUD_broadcast_create(p_name)
  quake.scoreboard_create(arena, p_name)

  if arena.teams_enabled then
    quake.HUD_teams_score_create(p_name)
    panel_lib.get_panel(p_name, "quake_teams_score"):show()
  end

  player:get_meta():set_int("quake_weap_delay", 0)


  player:get_inventory():add_item("main", weapon)
  player:get_inventory():add_item("main", weapon2)

  player:set_physics_override({
            speed = 2.3,
            jump = 1.5,
            gravity = 1.15,
            sneak_glitch = true,
            new_move = true
            })

  minetest.sound_play("quake_voice_fight", {
    to_player = p_name,
  })

  minetest.after(0.01, function()
    quake.scoreboard_update(arena)

    if arena.teams_enabled then
      quake.HUD_teams_score_update(arena, p_name)
    end

    if arena.kill_leader ~= "" then
      quake.HUD_stats_update(arena, p_name, "kill_leader")
    end
  end)

end)



arena_lib.on_celebration("quake", function(arena, winner_name)

  --quake.add_xp(winner_name, 50)

  minetest.after(0.01, function()
    for pl_name, stats in pairs(arena.players) do

      local inv = minetest.get_player_by_name(pl_name):get_inventory()

      inv:remove_item("main", ItemStack("quake:railgun"))
      inv:remove_item("main", ItemStack("quake:rocket_launcher"))

      inv:add_item("main", ItemStack("quake:match_over"))
      panel_lib.get_panel(pl_name, "quake_scoreboard"):show()
    end
  end)
end)



arena_lib.on_end("quake", function(arena, players)

  for pl_name, stats in pairs(players) do

    local stats = panel_lib.get_panel(pl_name, "quake_stats")
    local scoreboard = panel_lib.get_panel(pl_name, "quake_scoreboard")

    stats:remove()
    scoreboard:remove()
    quake.HUD_broadcast_remove(pl_name)

    if arena.teams_enabled then
      local team_score = panel_lib.get_panel(pl_name, "quake_teams_score")
      team_score:remove()
    end

    quake.update_storage(pl_name)

    -- se non c'è hub_manager, resetto la fisica
    if not minetest.get_modpath("hub_manager") then
      minetest.get_player_by_name(pl_name):set_physics_override({
                speed = 1,
                jump = 1,
                gravity = 1,
                sneak_glitch = false
                })
    end
  end
end)



arena_lib.on_death("quake", function(arena, p_name, reason)
  quake.HUD_stats_update(arena, p_name, "deaths")
  arena.players[p_name].killstreak = 0

  -- se muoio suicida, perdo una kill
  if reason.type == "fall" or reason.player_name == p_name then

    local p_stats = arena.players[p_name]

    if arena.teams_enabled then
      local team = arena.teams[p_stats.teamID]
      team.deaths = team.deaths + 1
    end

    p_stats.kills = p_stats.kills - 1
    quake.HUD_stats_update(arena, p_name, "kills")
    quake.scoreboard_update(arena)
    quake.subtract_exp(p_name, 10)

    -- ricalcolo kill leader se ero kill leader
    if arena.kill_leader == p_name then
      quake.calc_kill_leader(arena, p_name)
      for pl_name, stats in pairs(arena.players) do
        quake.HUD_stats_update(arena, pl_name, "kill_leader")
      end
    end
  end

end)



arena_lib.on_quit("quake", function(arena, p_name)

  local stats = panel_lib.get_panel(p_name, "quake_stats")
  local scoreboard = panel_lib.get_panel(p_name, "quake_scoreboard")

  stats:remove()
  scoreboard:remove()
  quake.HUD_broadcast_remove(p_name)

  if arena.teams_enabled then
    local team_score = panel_lib.get_panel(p_name, "quake_teams_score")
    team_score:remove()
  end

  -- se non c'è hub_manager, resetto la fisica
  if not minetest.get_modpath("hub_manager") then
    minetest.get_player_by_name(p_name):set_physics_override({
              speed = 1,
              jump = 1,
              gravity = 1,
              sneak_glitch = false
              })
  end
end)
