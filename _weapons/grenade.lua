quake.register_grenade("quake:grenade", {
  description = "grenade",
  mesh = "quake_grenade.obj",
  tiles = {"quake_grenade.png"},
  wield_scale = {x=1.5, y=1.5, z=1.5},
  inventory_image = "quake_grenade_icon.png",
  stack_max = 99,

  entity = {
    throw_speed = 17,
    explosion_damage = 16,
    physical = true,
    collide_with_objects = true,
    visual = "mesh",
    --wield_item = "quake:grenade",
    visual_size = {x=7, y=7, z=7},
    mesh = "quake_grenade.obj",
    explosion_texture = "quake_rocket_particle.png",
    textures = {"quake_grenade.png"},
    collisionbox = {-0.2, -0.2, -0.2, 0.2, 0.2, 0.2},
    explosion_range = 4,
    duration = 1.5,
    gravity = true,
    on_destroy = quake.grenade_explode,
    particle = {
  		image = "quake_bullet_rocket.png",
  		life = 1,
  		size = 2,
  		glow = 0,
  		interval = 5,
  	},

  },

})
