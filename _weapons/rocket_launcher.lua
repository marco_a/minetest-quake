
quake.register_weapon("quake:rocket_launcher", {
  description = S("Rocket Launcher"),
  mesh = "quake_rocketlauncher.obj",
  tiles = {"quake_rocketlauncher.png"},
  wield_scale = {x=1.3, y=1.3, z=1.3},
  inventory_image = "quake_rocketlauncher_icon.png",
  weap_damage = 10,
  weap_delay = 0.8,
  weap_sound_shooting = "quake_rocketlauncher_shoot",
  is_hitscan = false,
  range = 100,
  bullet = "quake:rocket",
  knockback = true

})
