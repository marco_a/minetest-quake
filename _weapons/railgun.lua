
quake.register_weapon("quake:railgun", {
  description = S("Railgun"),
  mesh = "quake_railgun.obj",
  tiles = {"quake_railgun.png"},
  wield_scale = {x=1.3, y=1.3, z=1.3},
  inventory_image = "quake_railgun_icon.png",
  weap_damage = 777,        -- FF7 in my heart
  weap_delay = 0.9,
  weap_sound_shooting = "quake_railgun_shoot",
  is_hitscan = true,
  range = 100
})
