# Minetest Quake

>  **BEWARE: THE PROJECT HAS BEEN ABANDONED AND IT HAS BEEN TURNED INTO [Block League](https://gitlab.com/zughy-friends-minetest/block_league). Arena_lib settings on here are outdated, so if anyone wants to fork it, please keep that in mind**


Quake mod for Minetest

### Dependencies
* [achievements_lib](https://gitlab.com/zughy-friends-minetest/achievements_lib) by me
* [arena_lib](https://gitlab.com/zughy-friends-minetest/arena_lib/) by me and friends
* (already inside) [ChatCMDBuilder](https://github.com/rubenwardy/ChatCmdBuilder/) by rubenwardy
* [controls](https://github.com/Arcelmi/minetest-controls) by Arcelmi
* [panel_lib](https://gitlab.com/zughy-friends-minetest/panel_lib) by me and friends


### Resources
2D Graphic assets by me  
3D models by Scarecrow  
Railgun shooting sound by [kafokafo](https://freesound.org/people/kafokafo/sounds/128229/)  
Rocket launcher shooting sound by [Audionautics](https://freesound.org/people/Audionautics/sounds/171655/)  
Shotgun shooting sound by [coolguy](https://freesound.org/people/coolguy244e/sounds/266977/)  
Shotgun reloading sound by [jeseid77](https://freesound.org/people/jeseid77/sounds/86246/)  
Hit sound by [cabled_mess](https://freesound.org/people/cabled_mess/sounds/350926/)  
Frag sound by [jmayoff](https://freesound.org/people/jmayoff/sounds/255156/)  
Voice acting clips by [EFlexMusic](https://freesound.org/people/EFlexMusic/)  
  
Most audio files have been tweaked by me

---

Images and models are under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

