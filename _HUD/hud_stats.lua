function quake.HUD_stats_create(p_name)

  local panel = Panel:new({
      name = "quake_stats",
      player = p_name,
      bg = "",
      position = { x = 0, y = 1 },
      alignment = { x = -1, y = 0 },
      title = "",

      sub_img_elems = {
        killleader_bg = {
          scale     = { x = 1.3, y = 1 },
          offset    = { x = 10, y = -90 },
          alignment = { x = 1, y = 0 },
          text      = "quake_hud_bg.png"
        },
        killleader_icon = {
          scale     = { x = 2, y = 2 },
          offset    = { x = 10, y = -90 },
          alignment = { x = 1, y = 0 },
          text      = "quake_hud_killleader.png",
          z_index   = 1
        },
        kills_bg = {
          scale     = { x = 0.45, y = 1 },
          offset    = { x = 10, y = -55 },
          alignment = { x = 1, y = 0 },
          text      = "quake_hud_bg.png"
        },
        kills_icon = {
          scale     = { x = 2, y = 2 },
          offset    = { x = 10, y = -55 },
          alignment = { x = 1, y = 0 },
          text      = "quake_hud_kills.png",
          z_index   = 1
        },
        deaths_bg = {
          scale     = { x = 0.45, y = 1 },
          offset    = { x = 10, y = -20 },
          alignment = { x = 1, y = 0 },
          text      = "quake_hud_bg.png"
        },
        deaths_icon = {
          scale     = { x = 2, y = 2 },
          offset    = { x = 10, y = -20 },
          alignment = { x = 1, y = 0 },
          text      = "quake_hud_deaths.png",
          z_index   = 1
        }
      },

      sub_txt_elems = {
        killleader_txt = {
          alignment = { x = 1, y = 0 },
          offset    = { x = 50, y = -90 },
          text      = "0 |"
        },
        kills_txt = {
          alignment = { x = 1, y = 0 },
          offset    = { x = 50, y = -55 },
          text      = "0"
        },
        deaths_txt = {
          alignment = { x = 1, y = 0 },
          offset    = { x = 50, y = -20 },
          text      = "0"
        }
      }
    })

end



function quake.HUD_stats_update(arena, p_name, stat)

  local panel = panel_lib.get_panel(p_name, "quake_stats")

  if stat == "kill_leader" then
    local kill_leader = arena.kill_leader
    panel:update(nil,
    {killleader_txt = {
      text = arena.players[kill_leader].kills .. " | " .. kill_leader
    }})

  elseif stat == "kills" then
    panel:update(nil,
    {kills_txt = {
      text = arena.players[p_name].kills
    }})

  elseif stat == "deaths" then
    panel:update(nil,
    {deaths_txt = {
      text = arena.players[p_name].deaths
    }})
  end

end
