quake.players = {}      -- KEY: p_name, INDEX: {lv (int), xp (int), kills (int), time_playing (int)}

local storage = minetest.get_mod_storage()



function quake.init_storage()

  -- carico tutti i giocatori
  for pl_name, pl_stats in pairs(storage:to_table().fields) do
    quake.players[pl_name] = minetest.deserialize(pl_stats)
  end

end



function quake.add_player_to_storage(p_name)
  quake.players[p_name] = {LV = 0, XP = 0, KILLS = 0, TIME_PLAYING = 0}
  storage:set_string(p_name, minetest.serialize(quake.players[p_name]))
end



function quake.update_storage(p_name)
  storage:set_string( p_name, minetest.serialize(quake.players[p_name]))
end



function quake.is_player_in_storage(p_name)
  return storage:get_string(p_name) ~= ""
end
