quake = {}
local S = minetest.get_translator("quake")

arena_lib.register_minigame("quake", {
  prefix = "[Quake] ",
  teams = {
    S("red"),
    S("blue")
  },
  teams_color_overlay = {
    "red",
    "blue"
  },
  hub_spawn_point = { x = 8, y = 6, z = 4 },
  join_while_in_progress = true,
  celebration_time = 5,
  disabled_damage_types = {
    "fall"
  },
  properties = {
    kill_cap = 10
  },
  temp_properties = {
    kill_leader = "",
    first_blood = ""
  },
  team_properties = {
    kills = 0,
    deaths = 0,
  },
  player_properties = {
    killstreak = 0
  }
})

--arena_lib.update_properties("quake")

-- load other scripts

dofile(minetest.get_modpath("quake") .. "/achievements.lua")
dofile(minetest.get_modpath("quake") .. "/chatcmdbuilder.lua")
dofile(minetest.get_modpath("quake") .. "/commands.lua")
dofile(minetest.get_modpath("quake") .. "/database_manager.lua")
dofile(minetest.get_modpath("quake") .. "/exp_manager.lua")
dofile(minetest.get_modpath("quake") .. "/input_manager.lua")
dofile(minetest.get_modpath("quake") .. "/items.lua")
dofile(minetest.get_modpath("quake") .. "/player_manager.lua")
dofile(minetest.get_modpath("quake") .. "/privs.lua")
dofile(minetest.get_modpath("quake") .. "/utils.lua")
dofile(minetest.get_modpath("quake") .. "/_arena_lib/arena_manager.lua")
dofile(minetest.get_modpath("quake") .. "/_arena_lib/arena_properties.lua")
dofile(minetest.get_modpath("quake") .. "/_HUD/hud_achievements.lua")
dofile(minetest.get_modpath("quake") .. "/_HUD/hud_broadcast.lua")
dofile(minetest.get_modpath("quake") .. "/_HUD/hud_scoreboard.lua")
dofile(minetest.get_modpath("quake") .. "/_HUD/hud_stats.lua")
dofile(minetest.get_modpath("quake") .. "/_HUD/hud_teams_score.lua")
dofile(minetest.get_modpath("quake") .. "/_weapons/bullets.lua")
dofile(minetest.get_modpath("quake") .. "/_weapons/throwables.lua")
dofile(minetest.get_modpath("quake") .. "/_weapons/weapons.lua")
dofile(minetest.get_modpath("quake") .. "/_weapons/weapons_utils.lua")
dofile(minetest.get_modpath("quake") .. "/_weapons/railgun.lua")
dofile(minetest.get_modpath("quake") .. "/_weapons/rocket_launcher.lua")
dofile(minetest.get_modpath("quake") .. "/_weapons/shotgun.lua")
dofile(minetest.get_modpath("quake") .. "/_weapons/rocket.lua")
dofile(minetest.get_modpath("quake") .. "/_weapons/grenade.lua")


quake.init_storage()
